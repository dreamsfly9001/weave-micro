﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Server.Kestrel.Https;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using WeaveMicroClient;
using wRPC;
using wRPCService;

namespace gateway
{
    public class WeaveGateWay
    {
        public static MicroClient mc;
        public static string applicationUrl;
     public   void init(IConfigurationBuilder builder)
        {
            AppContext.SetSwitch("Microsoft.AspNetCore.Server.Kestrel.EnableWindows81Http2", true);
            //server[] servers= Funconfig.getConfig();
            //server ser= WeightAlgorithm.Get(servers, "abcd/ff");
            Console.WriteLine("Running WeaveMicro网关.");
            var config = builder.Build();
            var distributed = Convert.ToBoolean( config["distributed"]);
            String[] applicationUrls = config["applicationUrl"].Replace("http://", "").Replace("https://", "").Split(',');
            CallServer.distributed=distributed;
            if (distributed)
            {
                String mcip = config["Microcenter"];
                if (mcip != "")
                {
                    mc = new MicroClient(mcip.Split(':')[0], Convert.ToInt32(mcip.Split(':')[1]));
                    mc.ReceiveEvent += Mc_ReceiveEvent;
                    mc.Connection();
                }
                else
                {
                    Proccessor.servers = Funconfig.getConfig();
                }
                if (mcip != "")
                {
                    string bip = string.IsNullOrWhiteSpace(config["bindIP"]) ? null : config["bindIP"];
                    foreach (string applicationUrl in applicationUrls)
                        mc.RegClient("网关1", bip ?? applicationUrl.Split(':')[0], Convert.ToInt32(applicationUrl.Split(':')[1]));
                }
            }
            else
            {

                CallServer.keyValuePairs = ToolLoad.Load(CallServer.keyValuePairs);
                var sric=   ToolLoad.GetService();

                server ser = new server(); 
                ser.services = sric;
                ser.IP = "127.0.0.1";
                ser.Port = 1111;
                Proccessor.servers = new server[] { ser };
                if (Proccessor.servers != null)
                    foreach (server sera in Proccessor.servers)
                    {
                        foreach (service serice in sera.services)
                        {
                            if (!sera.servicesDic.ContainsKey(serice.Route))
                                sera.servicesDic.GetOrAdd(serice.Route, serice);
                        }
                    }

            }

            string zhengshu = config["CA"];// "10.133.17.81.cer";//server.pfx



            applicationUrl = config["applicationUrl"];
             var  args = new string[] { config["applicationUrl"].Replace(",",";") };
            Console.WriteLine(AppDomain.CurrentDomain.BaseDirectory + zhengshu);
           
            Proccessor.filetype = config["filetype"].Split(",");
            //AppContext.SetSwitch("Microsoft.AspNetCore.Server.Kestrel.EnableWindows81Http2", true);
            if (string.IsNullOrEmpty(zhengshu))
            {
                CreateHostBuilder(args).UseKestrel(options =>
                {

                }).Build().Run();
            }
            else
            {
                X509Certificate2 certificate;
                if (config["httpspassword"] == "")
                    certificate = new X509Certificate2(AppDomain.CurrentDomain.BaseDirectory + zhengshu);
                else
                    certificate = new X509Certificate2(AppDomain.CurrentDomain.BaseDirectory + zhengshu, config["httpspassword"]);
                CreateHostBuilder(args).UseKestrel(options =>
                {

                    options.ConfigureHttpsDefaults(
                        options =>
                        {

                            options.SslProtocols = SslProtocols.Tls11 | SslProtocols.Tls12 | SslProtocols.Tls13;
                            options.ClientCertificateMode = ClientCertificateMode.AllowCertificate;
                            options.ServerCertificate = certificate;

                        });


                }).Build().Run();
            }

            //mainthread loop
        
            if (mc != null) mc.Stop();
        }

        private  void Mc_ReceiveEvent(WeaveMicroClient.server[] serv)
        {
            try
            {
                String datastr = Newtonsoft.Json.JsonConvert.SerializeObject(serv);
                datastr = "{\"server\":" + datastr + "}";
                System.IO.StreamWriter sw = new System.IO.StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "funconfig.json", false);
                sw.Write(datastr);
                sw.Close();
                Proccessor.servers = Funconfig.getConfig();
                WeightAlgorithm._serviceDic.Clear();
            }
            catch (Exception e) { Console.WriteLine(e.Message); }
        }

        //static IConfigurationBuilder builder = new ConfigurationBuilder().SetBasePath(AppDomain.CurrentDomain.BaseDirectory).AddJsonFile("config.json");

        public static IWebHostBuilder CreateHostBuilder(string[] args) => WebHost.CreateDefaultBuilder().UseUrls(args[0]).UseStartup<Startup>();
    }
}