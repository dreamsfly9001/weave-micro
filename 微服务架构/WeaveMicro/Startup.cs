using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using WeaveDoc;

namespace WeaveMicro
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        static IConfigurationBuilder builder = new ConfigurationBuilder().SetBasePath(AppDomain.CurrentDomain.BaseDirectory).AddJsonFile("config.json");
        public static IConfigurationRoot config;
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            config = builder.Build();
            services.AddRazorPages();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            string contentRoot = AppDomain.CurrentDomain.BaseDirectory;
            string runRoot = AppDomain.CurrentDomain.BaseDirectory;
            //静态文件浏览支持，默认路径为www映射到Static。根据MIME配置添加支持的文件格式
            var mime = new FileExtensionContentTypeProvider().Mappings;
            mime[".apk"] = "application/vnd.android.package-archive";
            var mimeCustom = config.GetSection("MIME")?.Get<Dictionary<string, string>>() ?? new Dictionary<string, string>();
            foreach (var m in mimeCustom)
            {
                if (string.IsNullOrWhiteSpace(m.Value)) mime.Remove(m.Key);
                else mime[m.Key] = m.Value;
            }
            IFileProvider fileProvider = new PhysicalFileProvider(Path.Combine(contentRoot, config.GetSection("StaticFilePath")?.Get<string>() ?? @"doc"));//静态文件存储目录
            IFileProvider runProvider = new PhysicalFileProvider(runRoot);//运行目录

            string reqPath = runRoot.Replace(contentRoot, "").Replace("\\", "/").TrimEnd('/');
            //  app.UseHttpsRedirection();
            //app.UseMiddleware<CorsMiddleware>();
            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = fileProvider,
                RequestPath = config.GetSection("StaticFileRequestPath")
                    ?.Get<string>()?.TrimEnd('/') ?? @"/documents",
                ContentTypeProvider = new FileExtensionContentTypeProvider(mime)
            }).UseStaticFiles(new StaticFileOptions
            {
                FileProvider = runProvider,
                RequestPath = reqPath,
                ContentTypeProvider = new FileExtensionContentTypeProvider(mime)
            });
            //app.UseFileServer(new FileServerOptions()//提供文件目录访问形式
            //{
            //    FileProvider = new PhysicalFileProvider(Path.Combine(contentRoot, "wwwroot", "apiHtml")),
            //    RequestPath = "/apiHtml",
            //    EnableDirectoryBrowsing = true//是否启用目录的属性
            //});

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
            });
            //使用API文档
            app.UseDoc();
        }
    }
}
