﻿using gateway;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.AspNetCore.Server.Kestrel.Https;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Runtime.ConstrainedExecution;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Xml.Linq;

namespace SSL_Test
{
    internal class Program
    {
        static IConfigurationBuilder builder = new ConfigurationBuilder().SetBasePath(AppDomain.CurrentDomain.BaseDirectory).AddJsonFile("config.json");

        public static IWebHostBuilder CreateHostBuilder(string[] args) => WebHost.CreateDefaultBuilder().UseUrls(args[0]).UseStartup<Startup>();
        static void Main(string[] args)
        {
            AppContext.SetSwitch("Microsoft.AspNetCore.Server.Kestrel.EnableWindows81Http2", true);
            var config = builder.Build();

          
            string zhengshu = config["CA"];// "10.133.17.81.cer";//server.pfx



          var  applicationUrl = config["applicationUrl"];
            args = new string[] { config["applicationUrl"].Replace(",", ";") };
            Console.WriteLine(AppDomain.CurrentDomain.BaseDirectory + zhengshu);
            X509Certificate2 certificate;
            if (config["httpspassword"] == "")
                certificate = new X509Certificate2(AppDomain.CurrentDomain.BaseDirectory + zhengshu);
            else
                certificate = new X509Certificate2(AppDomain.CurrentDomain.BaseDirectory + zhengshu, config["httpspassword"]);
            //AppContext.SetSwitch("Microsoft.AspNetCore.Server.Kestrel.EnableWindows81Http2", true);
            CreateHostBuilder(args).UseKestrel(options =>
            {

                options.ConfigureHttpsDefaults(
                    options => {

                        options.SslProtocols = SslProtocols.Tls11 | SslProtocols.Tls12 | SslProtocols.Tls13;
                        options.ClientCertificateMode = ClientCertificateMode.AllowCertificate;
                        options.ServerCertificate = certificate;

                    });


            }).Build().Run();

        }
    }
}
